# AlIBABA-COUNTIRES-PWA

##Nariman Palangi
## Demo 

ALIBABA-COUNTRIES [DEMO](http://alibaba-countries.gigfa.com/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

___________________________________________________________________________
### APP STRUCTURE

#### # api             | this is api layer of the app
#### # assets
#### # components
#### # const           | constance used by components, apis, ...
#### # layouts         
#### # locales         | translate files
#### # mixins           
#### # plugins          
#### # router
#### # store
#### # styles         
#### # tools           | app configs and global functions 
#### # views


_________________________________________________________________
### Tools 

### 1.Api.js
axios configuration
config on request 
```
    api.interceptors.request.use()
```
handle response and error with passing api response to Handler class
```
    api.interceptors.response.use()
```

### 2.App.js

main app configuration

#### app_name
set global app name

#### supper_key
local storage pre-fix key, add at start of each key on local storage

#### fonts

this object sets font for each locale in the app \


\
example: 
```

  fonts: {
    en: "'Nunito Sans', sans-serif !important;",
    fa: "IranYekan, Tahoma, sans-serif !important;",
  },
```

#### theme_mode

set default app theme
theme_mode values "dark","light"


#### default_lang
set default app language

#### test_data
set it to true if you want to call api with mock data 

### 3.Handler.js

handler Object \

```
     error()
```

handle errors return from api

```
    response()
```
handle response return from api

### 4.Lang.js

add a wrapper to translator library

### 5.Message.js

Handle all message and pass message to global snackbar

### 6.Mock
Handle Mock data for testing api

```
     LOCAL_DATA: {
    /*'/api/v1/auth/login_or_register' : {"success":1,"error":"","messages":[],"data":[]},*/
    },
```

add api address and response here

### 7.Storage.js

Local storage manager

```
    set(key,value)
```
For setting data in local storage

```
    get(key)
```

get data from local storage

### 8.Urls.js
Manage all api addresses

Example
```
const list = {
  base_url: process.env.VUE_APP_BASE_URL || "https://restcountries.eu/rest/v2/",

  /*
  countries routes
  */
  countries: "all",
  regions: "region/%s",
  name: "name/%s",
  code: "alpha/%s",
};
```

#### base_url
set base url of api