import api from "@/tools/Api";
import urls from "@/tools/Urls";
import { RETURNED_FIELDS } from "@/const/countries_api_returned_fields.const";

/*
 *  get all countries
 *  ------------------------
 *  Api address | /rest/v2/all
 *  ------------------------
 *  @params config {Array} filter the output of your request to include only the specified fields.
 *  ------------------------
 *  method : Get
 *  ------------------------
 *  Description :
 *  ------------------------
 *  get all countries
 *  ------------------------
 *  @return Promise
 *  ------------------------
 */
export const getCountries = function (config = RETURNED_FIELDS) {
  return api.get(urls.api("countries"), {
    params: {
      fields: config.join(";"),
    },
  });
};
