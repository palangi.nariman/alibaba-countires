import api from "@/tools/Api";
import urls from "@/tools/Urls";
import { RETURNED_FIELDS } from "@/const/countries_api_returned_fields.const";

/*
 *  get country by ISO 3166-1 2-letter or 3-letter country code
 *  ------------------------
 *  Api address | /rest/v2/alpha/{code}
 *  ------------------------
 *  @param code {String}
 *  @params config {Array} filter the output of your request to include only the specified fields.
 *  ------------------------
 *  method : Get
 *  ------------------------
 *  Description :
 *  ------------------------
 *  get country by ISO 3166-1 2-letter or 3-letter country code
 *  ------------------------
 *  @return Promise
 *  ------------------------
 */
export const getCountriesByCode = function (code, config = RETURNED_FIELDS) {
  return api.get(urls.api("code", [`${code}`]), {
    params: {
      fields: config.join(";"),
    },
  });
};
