import api from "@/tools/Api";
import urls from "@/tools/Urls";
import { RETURNED_FIELDS } from "@/const/countries_api_returned_fields.const";

/*
 *  get all countries by keyword in their names
 *  ------------------------
 *  Api address | /rest/v2/name/{keyword}
 *  ------------------------
 *  @param keyword {String}
 *  @params config {Array} filter the output of your request to include only the specified fields.
 *  ------------------------
 *  method : Get
 *  ------------------------
 *  Description :
 *  ------------------------
 *  get all countries by keyword in their names
 *  ------------------------
 *  @return Promise
 *  ------------------------
 */
export const getCountriesByKeyword = function (
  keyword,
  config = RETURNED_FIELDS
) {
  return api.get(urls.api("name", [`${keyword}`]), {
    params: {
      fields: config.join(";"),
    },
  });
};
