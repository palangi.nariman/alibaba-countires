import api from "@/tools/Api";
import urls from "@/tools/Urls";
import { RETURNED_FIELDS } from "@/const/countries_api_returned_fields.const";

/*
 *  get all countries with same region
 *  ------------------------
 *  Api address | /rest/v2/region/{region}
 *  ------------------------
 *  @param region {String}
 *  @param config {Array} filter the output of your request to include only the specified fields.
 *  ------------------------
 *  method : Get
 *  ------------------------
 *  Description :
 *  ------------------------
 *  get all countries with same region
 *  ------------------------
 *  @return Promise
 *  ------------------------
 */
export const getCountriesByRegion = function (
  region,
  config = RETURNED_FIELDS
) {
  return api.get(urls.api("regions", [`${region}`]), {
    params: {
      fields: config.join(";"),
    },
  });
};
