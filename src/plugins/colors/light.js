import colors from "vuetify/lib/util/colors";
export default {
  black: "#111517", // - Very Dark Blue (Light Mode Text): hsl(200, 15%, 8%)
  white: colors.white, // white
  primary: "#FFFFFF", // - Dark Blue (Dark Mode Elements): hsl(209, 23%, 22%)
  accent: "#858585", //- Dark Gray (Light Mode Input): hsl(0, 0%, 52%)
  secondary: "#FAFAFA", // - Very Light Gray (Light Mode Background): hsl(0, 0%, 98%)
  typo: "#111517", // - Very Dark Blue (Light Mode Text): hsl(200, 15%, 8%)
  warning: colors.amber, // amber
  error: "#FF6333", // deepOrange accent-4
  success: colors.green.darken2, // green darken-2
  info: colors.teal.lighten1, // teal lighten-1
  more: colors.lightBlue.darken1, // light-blue darken-1
};
