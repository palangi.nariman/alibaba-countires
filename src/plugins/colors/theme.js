import light from "./light";
import dark from "./dark";
import app from "@/tools/App";
export default {
  dark: app.theme_mode === "dark",
  options: {
    customProperties: true,
  },
  themes: {
    light,
    dark,
  },
};
