import colors from "vuetify/lib/util/colors";
export default {
  black: colors.black, // black
  white: colors.white, // - White (Dark Mode Text & Light Mode Elements): hsl(0, 0%, 100%)
  primary: "#2b3945", // - Dark Blue (Dark Mode Elements): hsl(209, 23%, 22%)
  secondary: "#202c37", // - Very Dark Blue (Dark Mode Background): hsl(207, 26%, 17%)
  accent: "#ffffff", // - Very Dark Blue (Dark Mode Background): hsl(207, 26%, 17%)
  typo: colors.white, // - White (Dark Mode Text & Light Mode Elements): hsl(0, 0%, 100%)
  warning: colors.amber, // amber
  error: "#FF6333", // deepOrange accent-4
  success: colors.green.darken2, // green darken-2
  info: colors.teal.lighten1, // teal lighten-1
  more: colors.lightBlue.darken1, // light-blue darken-1
};
