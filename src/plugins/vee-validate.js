import { _t } from "@/tools/Utils";
import { extend, configure } from "vee-validate";
// import urls from "@/tools/Urls";
// import api from "@/tools/Api";
import { required } from "vee-validate/dist/rules";
configure({
  defaultMessage: (field, values) => {
    // override the field name.
    values._field_ = _t(`fields.${field}`);

    return _t(`validation.${values._rule_}`, values);
  },
});

// Install required rule and message.
extend("required", required);
