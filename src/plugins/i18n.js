import Vue from "vue";
import VueI18n from "vue-i18n";
import app from "../tools/App";

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: app.default_lang || process.env.VUE_APP_I18N_LOCALE,
  fallbackLocale: "en" || process.env.VUE_APP_I18N_FALLBACK_LOCALE,
  silentLocallationWarn: true,
  formatFallbackMessages: true,
  messages: require("./../locales"),
});

export default i18n;
