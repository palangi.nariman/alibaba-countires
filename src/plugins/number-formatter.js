export default {
  install(Vue) {
    // an instance method
    Vue.prototype.$_number = function (number, local = "en-US") {
      return new Intl.NumberFormat(`${local}`).format(number);
    };
  },
};
