import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./plugins/i18n";
import translator from "./plugins/translator";
import number_formatter from "./plugins/number-formatter";

import "./registerServiceWorker";
import "./plugins/vee-validate";

import goBackNavigator from "./plugins/goBackNavigator";
import vuetify from "./plugins/vuetify";
import VueGtag from "vue-gtag";

Vue.use(
  VueGtag,
  {
    config: { id: "UA-284348315" },
  },
  router
);

Vue.config.productionTip = false;
Vue.use(translator);
Vue.use(number_formatter);

Vue.use(goBackNavigator);
require("@/router/middlewares");

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
