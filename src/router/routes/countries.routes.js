"use strict";
import { _t } from "@/tools/Utils";

export const countriesRoutes = [
  {
    path: "countries",
    name: "countries",
    component: () =>
      import(/* webpackChunkName: "countries" */ "@/views/countries/index"),
    meta: {
      title: _t("router.routes.countries_routes.index"),
    },
  },
  {
    path: "countries/:id",
    name: "countries.details",
    component: () =>
      import(/* webpackChunkName: "countries" */ "@/views/countries/detail"),
    meta: {
      title: _t("router.routes.countries_routes.detail"),
    },
  },
];
