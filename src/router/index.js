import Vue from "vue";
import VueRouter from "vue-router";
import { _t } from "@/tools/Utils";
import { countriesRoutes } from "@/router/routes/countries.routes";
import { pageNotFoundRoutes } from "@/router/routes/pageNotFount.routes";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "root",

    component: () => import("@/layouts/App.vue"),
    redirect: { name: "countries" },
    meta: {
      title: _t("router.index.panel_title"),
      requiresAuth: false,
    },
    children: [...countriesRoutes],
  },
  ...pageNotFoundRoutes,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
