module.exports = {
  components: {
    app: {
      back_navigator: {
        buttons: {
          back: "Back",
        },
      },
      header: {
        title: "Where in the world",
      },
      theme_mode_changer: {
        light: "light mode",
        dark: "dark mode",
      },
      page_not_found: {
        title: "404",
        subtitle_part_one: "Sorry!",
        subtitle_part_two: "Page Not Found",
        buttons: {
          go_back: "Go Back",
        },
      },
    },
    countries: {
      filters: {
        region_filter: {
          placeholder: "Filter by region",
        },
        search: {
          placeholder: "search fo a country...",
        },
      },
      detail: {
        country_info: {
          not_found: "The country not found!",
        },
        country_spec: {
          items: {
            nativeName: "Native Name",
            population: "Population",
            region: "Region",
            subregion: "Subregion",
            capital: "Capital",
            topLevelDomain: "Top Level Domain",
            currencies: "Currencies",
            languages: "Languages",
          },
        },
        country_border_list: {
          title: "Border countries",
        },
      },
      list: {
        country: {
          population: "Population",
          region: "Region",
          capital: "Capital",
        },
      },
    },
  },
  constance: {
    region_const: {
      africa: "Africa",
      america: "America",
      asia: "Asia",
      europe: "Europe",
      oceania: "Oceania",
    },
  },
  router: {
    routes: {
      countries_routes: {
        index: "countries",
        detail: "Details",
      },
      page_not_found_routes: {
        not_found: "Page not found",
      },
    },
  },
  views: {},

  fields: {
    title: "Title",
  },
  validation: {
    required: "{_field_} can not be empty.",
  },
  "Not Found": "Not Found",
};
