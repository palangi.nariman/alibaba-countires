module.exports = {
  components: {
    app: {
      back_navigator: {
        buttons: {
          back: "برگشت",
        },
      },
      header: {
        title: "جایی در دنیا",
      },
      theme_mode_changer: {
        light: "مد روشن",
        dark: "مد تاریک",
      },
      page_not_found: {
        title: "404",
        subtitle_part_one: "متاسفم!",
        subtitle_part_two: "صفحه مورد نظر یافت نشد.",
        buttons: {
          go_back: "برگشت",
        },
      },
    },
    countries: {
      filters: {
        region_filter: {
          placeholder: "فیلتر قاره",
        },
        search: {
          placeholder: "جست و جو",
        },
      },
      detail: {
        country_info: {
          not_found: "کشور مورد نظر پیدا نشد!",
        },
        country_spec: {
          items: {
            nativeName: "اسم بومی",
            population: "جمعیت",
            region: "منطقه",
            subregion: "ناحیه",
            capital: "پایتخت",
            topLevelDomain: "دامنه سطح بالا",
            currencies: "واحد های پولی",
            languages: "زبان ها",
          },
        },
        country_border_list: {
          title: "کشورهای همسایه",
        },
      },
      list: {
        country: {
          population: "جمعیت",
          region: "منطقه",
          capital: "پایتخت",
        },
      },
    },
  },
  constance: {
    region_const: {
      africa: "آفریقا",
      america: "آمریکا",
      asia: "آسیا",
      europe: "اروپا",
      oceania: "اقیانوسیه",
    },
  },
  router: {
    routes: {
      countries_routes: {
        index: "لیست کشورها",
        detail: "جزییات کشور",
      },
      page_not_found_routes: {
        not_found: "صفحه مورد نظر یافت نشد.",
      },
    },
  },
  views: {},
  fields: {
    title: "عنوان",
  },
  validation: {
    required: "{_field_} الزامی است",
  },

  NotFound: "یافت نشد.",
};
