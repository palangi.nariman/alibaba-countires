import axios from "axios";
import handler from "./Handler";
import lang from "./Lang";

const api = axios.create({});

// add token to header
api.interceptors.request.use((request) => {
  if (lang.locale()) request.headers.common["Accept-Language"] = lang.locale();

  return request;
});

// response and error handler
api.interceptors.response.use(
  (response) => handler.response(response),
  (error) => handler.error(error)
);

export default api;
