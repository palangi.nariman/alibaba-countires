import storage from "./Storage";

// Application Class
// this class handle app control and settings
const app = {
  app_name: "ALIBABA",
  supper_key: "@app_system:",
  fonts: {
    en: "'Nunito Sans', sans-serif !important;",
    fa: "IranYekan, Tahoma, sans-serif !important;",
  },
  theme_mode: "dark",
  default_lang: "en",
  test_data: false,
  languages: ["en", "fa"],
  limit: 12,
  rtl_lang: ["fa"],
  lang_items: [
    {
      key: "fa",
      title: "فارسی",
    },
    {
      key: "en",
      title: "English",
    },
  ],

  // load data with key
  load(key) {
    return storage.get(this.supper_key + key);
  },

  // Save data with key
  save(key, value) {
    storage.set(this.supper_key + key, value);
  },
};

export default app;
