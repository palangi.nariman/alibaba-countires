import lang from "./Lang";
// Utils
// general and useful functions to project.
//

export function _t(key, values = null) {
  return lang.t(key, values);
}
