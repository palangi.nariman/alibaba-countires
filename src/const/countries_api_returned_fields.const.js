export const RETURNED_FIELDS = [
  "name",
  "capital",
  "population",
  "region",
  "flag",
  "alpha3Code",
];
