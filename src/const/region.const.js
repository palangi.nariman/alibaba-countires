export const REGIONS = [
  {
    text: "africa",
    value: "africa",
  },
  {
    text: "america",
    value: "americas",
  },
  { text: "asia", value: "asia" },
  {
    text: "europe",
    value: "europe",
  },
  {
    text: "oceania",
    value: "oceania",
  },
];
