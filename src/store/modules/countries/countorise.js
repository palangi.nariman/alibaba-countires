// initial state
import message from "@/tools/Message";
import {
  getCountriesByRegion,
  getCountries,
  getCountriesByKeyword,
  getCountriesByCode,
} from "@/api/countries/index";

import { RETURNED_FIELDS } from "@/const/countries_api_returned_fields.const";

const state = () => ({
  countries: [],
  country: null,
});

// getters
const getters = {
  countries: (state) => state.countries,
  country: (state) => state.country,
};

// mutations
const mutations = {
  setCountries(state, value) {
    state.countries = value;
  },
  setCountry(state, value) {
    state.country = value;
  },
  clearCountries(state) {
    state.countries = [];
  },
  clearCountry(state) {
    state.country = null;
  },
};

// actions
const actions = {
  /*
   *  get all countries
   *  ------------------------
   *  Api address | /rest/v2/all
   *  ------------------------
   *  @params config {Array} filter the output of your request to include only the specified fields.
   *  ------------------------
   *  method : Get
   *  ------------------------
   *  Description :
   *  ------------------------
   *  get all countries and store response on state.countries
   *  ------------------------
   *  @return Promise
   *  ------------------------
   */
  async fetchCountries({ commit }, config = RETURNED_FIELDS) {
    try {
      let response = await getCountries(config);
      commit("setCountries", response);
    } catch (err) {
      commit("clearCountries");
      message.error(err.response.data.message);
    }
  },

  /*
   *  get all countries with same region
   *  ------------------------
   *  Api address | /rest/v2/region/{region}
   *  ------------------------
   *  @param region {String}
   *  @param config {Array} filter the output of your request to include only the specified fields.
   *  ------------------------
   *  method : Get
   *  ------------------------
   *  Description :
   *  ------------------------
   *  get all countries with same region and store response on state.countries
   *  ------------------------
   *  @return Promise
   *  ------------------------
   */
  async fetchCountriesByRegion(
    { commit },
    { region, config = RETURNED_FIELDS }
  ) {
    try {
      let response = await getCountriesByRegion(region, config);
      commit("setCountries", response);
    } catch (err) {
      commit("clearCountries");
      message.error(err.response.data.message);
    }
  },

  /*
   *  get all countries by keyword in their names
   *  ------------------------
   *  Api address | /rest/v2/name/{keyword}
   *  ------------------------
   *  @param keyword {String}
   *  @params config {Array} filter the output of your request to include only the specified fields.
   *  ------------------------
   *  method : Get
   *  ------------------------
   *  Description :
   *  ------------------------
   *  get all countries by keyword in their names and store response on state.countries
   *  ------------------------
   *  @return Promise
   *  ------------------------
   */
  async fetchCountriesByName(
    { commit },
    { keyword, config = RETURNED_FIELDS }
  ) {
    try {
      let response = await getCountriesByKeyword(keyword, config);
      commit("setCountries", response);
    } catch (err) {
      commit("clearCountries");
      message.error(err.response.data.message);
    }
  },

  /*
   *  get country by ISO 3166-1 2-letter or 3-letter country code
   *  ------------------------
   *  Api address | /rest/v2/alpha/{code}
   *  ------------------------
   *  @param code {String}
   *  @params config {Array} filter the output of your request to include only the specified fields.
   *  ------------------------
   *  method : Get
   *  ------------------------
   *  Description :
   *  ------------------------
   *  get country by ISO 3166-1 2-letter or 3-letter country code and store response on state.country
   *  ------------------------
   *  @return Promise
   *  ------------------------
   */
  async fetchCountryByCode({ commit }, { code, config = RETURNED_FIELDS }) {
    commit("clearCountry");
    try {
      let response = await getCountriesByCode(code, config);
      commit("setCountry", response);
    } catch (err) {
      commit("clearCountry");
      message.error(err.response.data.message);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
