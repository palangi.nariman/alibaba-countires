// initial state
import vuetify from "@/plugins/vuetify";
const state = () => ({});

// getters
const getters = {};

// mutations
const mutations = {
  setTheme(state, value) {
    vuetify.framework.theme.dark = value;
  },
};

// actions
const actions = {
  toggleDarkTheme({ commit }) {
    commit("setTheme", !vuetify.framework.theme.isDark);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
