import Vue from "vue";
import Vuex from "vuex";

import loading from "@/store/modules/general/general.loading";
import messages from "@/store/modules/general/general.messages";
import lang from "@/store/modules/general/general.lang";
import theme from "@/store/modules/general/general.theme";

import countries from "@/store/modules/countries/countorise";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    loading,
    messages,
    lang,
    theme,
    countries,
  },
});
